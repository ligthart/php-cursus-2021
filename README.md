# PHP Backend Cursus

## Introductie

Welkom bij de basis cursus programmeren met PHP! 

Goed kunnen programmeren kan enkel geleerd worden indien je ook veel programmeert. 
Je kan ook niet leren bergbeklimmen door het boek "Hoe beklim ik een berg" te lezen! 

"Wat men moet leren doen, leert men door het te doen." -- Aristoteles

#### Wat ga je leren?

In deze cursus PHP leer je de basisbeginselen van het programmeren met PHP en leer je de syntax kennen. Met deze cursus leg je de basis die je nodig hebt om zelf met PHP web applicaties te maken. Je krijgt les van Dave, een developer met meer dan 15 jaar aan ervaring in het ontwikkelen van web applicaties.

#### Hoe ga je dit leren?

Deze cursus bestaat uit vijflessen met iederen een vijftal aan oefeningen en een opdracht.

Met iedere opdracht kom je steeds een stapje dichterbij het eindproduct dat we gaan bouwen.

## Voorbereiding

### Development Omgeving

Om niet te veel tijd kwijt te zijn aan het opzetten van een development omgeving heb ik een oplossing gemaakt (hieronder beschreven) die het je makkelijk maakt en waarmee je direct aan de slag kunt gaan.  

#### Snelstart Installatie

###### Git

Installeer git om deze repository te kunnen downloaden (`clonen` zoals het heet in git-termen):

- Github Desktop Client geschikt voor Windows en MacOS: https://desktop.github.com

###### Docker 

Installeer docker. Je hoeft nog niet te weten wat het is en wat het doet, gewoon installeren en opstarten!

Hiermee heb je binnen no-time een omgeving in de lucht op jouw machine.

Download en installeer docker voor jouw besturingssysteem:

- MacOS: https://hub.docker.com/editions/community/docker-ce-desktop-mac/
- Windows: https://hub.docker.com/editions/community/docker-ce-desktop-windows/
- Linux: https://hub.docker.com/search?q=&type=edition&offering=community&operating_system=linux

#### Starten

1. Open een terminal venster: 
	- In Windows is dat `CMD` of `PowerShell` (kun je vinden via Start > Zoeken ) 
	- In MacOS is dat `terminal` (kun je vinden via Spotlight > zoeken )
2. Ga naar de volgende directory: `cd lessen/omgeving`
3. Start de server:
	- Windows: Voer vervolgens dit commando uit: `docker compose up`
	- MacOS: `docker-compose up`
4. Jouw PHP development omgeving is nu opgestart, je kan dit controleren door te in je web-browser te navigeren naar `http://localhost`
5. De lessen vind je in `http://localhost/cursus/`

##### Alternatief

Mocht docker niet werken op jouw systeem dan kun je het volgende alternatief proberen:

https://www.mamp.info/

Stel de document root van de mamp web server in op de git repository folder `php-cursus-2021/lessen/omgeving/app/public/`.  Dit kan via het mamp configuratiepaneel.


#### Editor

Installeer een editor om de php bestanden mee te bewerken:

Enkele simpele editors:

- Windows: https://notepad-plus-plus.org/
- MacOS: https://www.sublimetext.com/3

### Leeromgeving

#### Kanalen

- Zoom: online meeting omgeving: 
- Slack: chat omgeving om contact te maken met de instructeur en cursisten: https://cursusomgeving.slack.com

### Inleveren opdrachten

Je kunt jouw werk uploaden naar een publieke "git" repository:

(zie Github Desktop Client)

Je bent vrij om te kiezen op welke je een account aanmaakt.

- gitlab
- github
- bitbucket

Stuur de link van jouw git repository naar mijn e-mailadres (zie onderaan).

Maak een duidelijke mappen structuur zodat ik de opdrachten makkelijk kan terugvinden. 

Bij voorkeur:

- weekopdrachten: les1, les2, les3, les4, les5
- eindopdracht
- readme.md waar je overige informatie kan plaatsen. 

```
.
├── weekopdrachten
│   ├── les1
│   └── les2
├── eindopdracht
│  
└── readme.md
```

### Naslagwerk 

Voor het raadplegen van informatie omtrent PHP kun je het volgende boek downloaden:

- https://goalkicker.com/PHPBook/

Wanneer je even snel iets wilt opzoeken kan dat via:

- https://www.php.net/

Of google is je gereedschap.

## Instructeur

Dave Ligthart <ligthart@pm.me>


