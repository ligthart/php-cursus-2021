### Doel: Maak een digitale variant in PHP van het Zeeslag Bordspel

De eerste speler die alle vijf de vijandelijke schepen tot zinken brengt, wint het spel.

Als alle gaatjes van een schip gevuld zijn met een roodpinnetje, dan zinkt het en wordt het van het oceaanrastergehaald. Vertel je tegenstander wat voor schip tot zinken is gebracht als je het van je oceaanraster haalt.

#### Onderdelen

- 2 rasters, 2 vloten van elk 5 schepen, rode en witte scorepinnetjes

Een vloot bestaat uit:

- Vliegdekmoederschip: 5 gaatjes
- Slagschip: 4 gaatjes
- Kruiser: 3 gaatjes
- Onderzeeër: 3 gaatjes
- Mijnenveger: 2 gaatjes

Een (gaatjes)raster bestaat uit:

Horizontaal: 1 t/m 10
Verticaal: A t/m J

#### Het spel

1. De spelers mogen nu om de beurt de coördinatennoemen van een doelwit - deze coördinaten bestaan altijd uit een letter en een getal, b.v. “H10”.
2. Als je beschoten wordt, moet je je tegenstander vertellen of het schot doel trof of miste. Een voltreffer markeer je op het schip op je oceaanraster met een rood pinnetje.
3. De speler die het schot afvuurde, markeert het op zijn doelraster; daarbij worden rode pinnetjes gebruikt voor voltreffers en witte pinnetjes voor schoten die misten.
