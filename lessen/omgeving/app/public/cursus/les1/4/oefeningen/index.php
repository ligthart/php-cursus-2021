<!DOCTYPE html>
<html>
<body>

<h1>Les 1.4 - Oefeningen</h1>

<p><em>* Je kunt de uitwerkingen plaatsen in een index.php bestand in de map "uitwerkingen".</em></p>

<h2>Oefening 1</h2>
<p>
Om één waarde uit de array te halen gebruik je de index van het element in de array. De index is gelijk aan de plaats van het element, geteld van links. De telling begint op 0. Om het derde getal te selecteren gebruik je dus als index 2. De notatie van de index werkt ook met vierkante haken [ ], geplaatst rechtstreeks achter een variabelenaam, met daartussen de index.
</p>

<blockquote>
$namen = array("Piet", "Kees", "Jan", "Annabel");
</blockquote>

<p>
Toon Piet en Annabel op het scherm.
</p>
<h2>Oefening 2</h2>

Verander de naam "Jan" in de array voor de naam "Karel".

<h2>Oefening 3</h2>

Maak een associatieve array obv onderstaande data:<br/>
<br/>
Product | Voorraad<br/>
-----------------<br/>
Kaas | 2<br/>
Melk | 4<br/>
Eieren | 16<br/>
Boter | 256<br/>

<h2>Oefening 4</h2>

Maak een multi-dimensionale array obv onderstaande data:<br/>
<br/>
ID | Product | Voorraad | Verkocht<br/>
---------------------------------- <br/>
1 | Kaas | 2 | 10 <br/>
2 | Melk | 4 | 8 <br/>
3 | Eieren | 16 | 14 <br/>
4 | Boter | 256 | 44 <br/>
<br/>
<h2>Oefening 5</h2>

Schrijf een functie die het aantal tekens teruggeeft van een string.

<h2>Oefening 6</h2>

Schrijf een functie die een woorden vervangt voor een ander woorden in een tekst.

<h2>Oefening 7</h2>

<p>
Schrijf een functie om onderstaande tekst om te zetten naar een array van woorden met kleine letters. <br/>
De woorden mogen geen leestekens bevatten zoals een '.' of een ','. <br/>
<br/>
*hint: gebruik de php "explode" functie.
</p>

<blockquote>
Cultivar a, trifecta instant skinny, espresso, con panna, crema spoon mocha, in coffee, sugar, french press medium latte trifecta instant to go. Breve skinny cinnamon grounds grinder, cortado, dark cup, crema percolator turkish, decaffeinated aromatic aftertaste redeye iced chicory. Single origin, steamed at seasonal, aged iced galão aftertaste beans sweet mug, extra  filter, in, cappuccino, white con panna, frappuccino aftertaste frappuccino qui chicory. Grinder medium et so, and java, trifecta, at, extra  café au lait trifecta, organic blue mountain coffee dark single origin. Viennese, aroma filter saucer cinnamon and, white, to go, crema coffee so lungo grounds, cultivar percolator french press acerbic americano siphon. Steamed eu est blue mountain, mug decaffeinated cortado strong, so as mug espresso acerbic sugar french press aroma.	
</blockquote>

</body>
</html>