<!DOCTYPE html>
<html>
<body>

<h1>Les 1.4 - Arrays, Functies, String functies</h1>

<h2> Arrays </h2>

<h3>Array</h3>

<blockquote>
	
	$autos = array("Volvo", "BMW", "Toyota"); <br/>

	echo "Ik hou van " . $autos[0] . ", " . $autos[1] . " en " . $autos[2];

</blockquote>

<h4>Nieuwe waarde aan een array toevoegen</h4>

<blockquote>
	$autos[] = "Mercedes";
</blockquote>

<h4>Een waarde verwijderen uit een array</h4>

<blockquote>
	$minderAutos = array_diff($autos, array("BMW"));
</blockquote>

<h3>Associatieve array</h3>

<blockquote>
	
	$leeftijden = array("Peter" => "31", "Ben" => "32"); <br/>

	echo "Peter is " . $leeftijden['Peter'] . " jaar oud.";

</blockquote>

<h3>Multi-dimensionale array</h3>

<blockquote>
	
$autos = array ( <br/>
&nbsp;&nbsp;array("Volvo",22,18), <br/>
&nbsp;&nbsp;array("BMW",15,13), <br/>
&nbsp;&nbsp;array("Saab",5,2), <br/>
&nbsp;&nbsp;array("Land Rover",17,15) <br/>
); <br/>
<br/>
echo "Merk: " . $autos[0][0]; 
<br/>
echo "In voorraad: " . $autos[0][1];
<br/>
echo "Verkocht: " . $autos[0][2];
<br/>
<br/>
echo "Merk: " . $autos[1][0];
<br/>
echo "In voorraad: " . $autos[1][1];
<br/>
echo "Verkocht: " . $autos[1][2];
<br/>
<br/>
// etc..

</blockquote>

<h2> Functies </h2>
<p>
Declareer een functie:
</p>
<blockquote>

function sayHelloWorld() { <br/>
&nbsp;&nbsp;return "Hello World"; <br/>
}<br/>
<br/>
echo sayHelloWorld();<br/>
	
</blockquote>

<p>
Met functie argumenten:
</p>
<blockquote>

function sayHelloWorld($first_name, $last_name = "") {  <br/>
&nbsp;&nbsp;return "Hello World, " . $first_name . "," . $last_name . !";<br/>
}<br/>
<br/>
echo sayHelloWorld("Kees", "de Boer");<br/>
	
</blockquote>

<h2> String functies </h2>

<blockquote>
$title = "Hallo Wereld"; <br/>
<br/>
$tekst = "Cultivar a, trifecta instant skinny, espresso, con panna, crema spoon mocha, in coffee, sugar, french press medium latte trifecta instant to go. Breve skinny cinnamon grounds grinder, cortado, dark cup, crema percolator turkish, decaffeinated aromatic aftertaste redeye iced chicory. Single origin, steamed at seasonal, aged iced galão aftertaste beans sweet mug, extra  filter, in, cappuccino, white con panna, frappuccino aftertaste frappuccino qui chicory. Grinder medium et so, and java, trifecta, at, extra  café au lait trifecta, organic blue mountain coffee dark single origin. Viennese, aroma filter saucer cinnamon and, white, to go, crema coffee so lungo grounds, cultivar percolator french press acerbic americano siphon. Steamed eu est blue mountain, mug decaffeinated cortado strong, so as mug espresso acerbic sugar french press aroma."
<br/>
// De lengte van de titel is:<br/> 
echo strlen($title);<br/>
<br/>
// De string omkeren: <br/>
echo strrev($title); <br/>
<br/>
// Vervangen van de woorden in een string: <br/>
echo str_replace('Hallo', 'Gegroet', $title); <br/>
<br/>
// Het aantal woorden in de tekst is: <br/>
echo str_word_count($tekst); <br/>
<br/>
// Het tonen van de waarde van een variabele in een string: <br/>
$wereld = "Allemaal";<br/>
echo "Hallo $wereld";<br/>

</blockquote>

<p>
Zie ook <a href="https://www.php.net/manual/en/ref.strings.php" target="_blank">php.net string functies</a>
<br/>

</body>
</html> 