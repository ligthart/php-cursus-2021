<!DOCTYPE html>
<html>
<body>
<h1>Les 1.5 - Loops</h1>
<!-- ============================ -->
<h2>De "For loop"</h2>
<p>
<?php

$words = array("hallo", "wereld,", "goedemorgen");

$sentence = '';

for ($index = 0; $index < count($words); $index++) {

	$sentence .= $words[$index];

	if ($index == count($words) - 1) {
		
		$sentence .= '!';
	} 
	else {
		
		$sentence .= ' ';
	}
}

echo $sentence;

?>
</p>

<h2>De "Foreach loop"</h2>
<p>
<?php 
foreach($words as $word) {
	echo $word . ' ';
}
?>!
</p>

<h2>De "While loop"</h2>
<p>
<?php 
$i = 0;
while(count($words) != $i) {
	echo strtoupper($words[$i]);
	echo ' ';
	$i++;
}
?>!
</p>

</body>
</html> 