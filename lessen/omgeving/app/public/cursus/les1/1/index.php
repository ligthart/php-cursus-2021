<!DOCTYPE html>
<html>
<body>

<h1>Les 1.1 - HTML en Echo statement</h1>

<h2>HTML</h2>

<blockquote>
&lt;!DOCTYPE html&gt; <br/>
&lt;html&gt; <br/>
&lt;body&gt; <br/>
&lt;h1&gt;Hallo Wereld&lt;/h1&gt; <br/>
&lt;/body&gt; <br/>
&lt;/html&gt; <br/>
</blockquote>

<h2>Tonen van <?php echo "Hallo Wereld"; ?></h2>
<p>

Je mag echo zo aanroepen: 

<blockquote> 
	&lt;php
	echo "Hallo Wereld";
	?&gt;
</blockquote>

Statements eindigen altijd met een <strong>;</strong> 

</p>

</body>
</html> 