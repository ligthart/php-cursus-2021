<!DOCTYPE html>
<html>
<body>

<h1>Les 1.3 - Data types, If statements en Operators</h1>

<h2>Data types</h2>

<blockquote>

// is een serie van characters mag tussen enkele of dubbele quotes. <br/>
$stringVariabele = "Hallo ik ben een string"; <br/>
<br/>
// heeft 1 getal en geen decimale punt tussen min -2,147,483,648 en max 2,147,483,647.<br/>
$intVariabele = 1; <br/>
<br/>
// een getal met een decimale punt.<br/>
$floatVariabele = 1.01; <br/>
<br/>
// true of false.<br/>
$booleanVariabele = true; <br/>
<br/>
// geen toegewezen waarde.<br/>
$nullVariabele = null; <br/>
<br/>
// je kan hier meerdere waarden opslaan in een variabele.<br/>
$arrayVariabele = array("Dit", "is", "een", "array"); <br/>

</blockquote>


<!-- Hier toon ik een lijstje met informatie over de variabelen -->
<h3> Informatie over de variabelen </h3>
<?php

$variabelen = array(
	"Hallo ik ben een string", 
	1, 
	1.01, 
	true, 
	null, 
	array("Dit", "is", "een", "array")
);

?>
<ul>
	<li>String: <?php var_dump($variabelen[0]); ?></li>
	<li>Integer: <?php var_dump($variabelen[1]); ?></li>
	<li>Float: <?php var_dump($variabelen[2]); ?></li>
	<li>Boolean: <?php var_dump($variabelen[3]); ?></li>
	<li>NULL: <?php var_dump($variabelen[4]); ?></li>
	<li>Array: <?php var_dump($variabelen[5]); ?></li>
</ul>

<h2>Code Comments</h2>

<blockquote>
/* <br/>
Met de functie 'var_dump' kun je informatie tonen  over de variabele.<br/>
<br/>
En zoals je ziet kun je op deze wijze commentaar toevoegen over<br/>
meerdere regels aan je code.<br/>
*/
</blockquote>

<?php
/* 
Met de functie 'var_dump' kun je informatie tonen  over de variabele.

En zoals je ziet kun je op deze wijze commentaar toevoegen over
meerdere regels aan je code.
*/
?>

<h2>If statements en operators</h2>

<h3>Vergelijkingsoperatoren</h3>

<p>
Een conditie vergelijkt twee waardes met elkaar en geeft true of false als waarde terug.
</p>

<blockquote>
$a = 1; <br/>
$b = 1; <br/>
if ($a == $b) { // gelijk aan <br/>
&nbsp;&nbsp;echo 'a == b'; <br/>
&nbsp;&nbsp;var_dump($a == $b); <br/>

} <br/> <br/>
$a = 2; <br/>
$b = 1;<br/>
if ($a > $b) { // groter dan maar niet gelijk<br/>
&nbsp;&nbsp;echo 'a > b';<br/>
&nbsp;&nbsp;var_dump($a > $b);<br/>

} <br/> <br/> 
$a = 1; <br/>
$b = 2;<br/>
if ($a < $b) { // kleiner dan maar niet gelijk <br/>
&nbsp;&nbsp;echo 'a < b';<br/>
&nbsp;&nbsp;var_dump($a < $b);<br/>

}<br/> <br/> 
$a = 2; <br/>
$b = 2;<br/>
if ($a >= $b) { // groter dan of gelijk<br/>
&nbsp;&nbsp;echo 'a >= b';<br/>
&nbsp;&nbsp;var_dump($a >= $b);<br/>

}<br/> <br/> 
$a = 2; <br/>
$b = 2;<br/>
if ($a <= $b) { // kleiner dan of gelijk<br/>
&nbsp;&nbsp;echo 'a <= b';<br/>
&nbsp;&nbsp;var_dump($a <= $b);<br/>

}<br/> <br/> 
$a = 1; <br/>
$b = 2;<br/>
if ($a <=> $b) {  // kleiner dan, gelijk, of groter dan<br/>
&nbsp;&nbsp;echo 'a <=> b';<br/>
&nbsp;&nbsp;var_dump($a <=> $b);<br/>

}<br/> <br/> 
if ($a != $b) { // niet gelijk aan<br/>
&nbsp;&nbsp;echo 'a != b';<br/>
&nbsp;&nbsp;var_dump($a != $b);<br/>

}<br/> <br/> 
</blockquote>

<h3>Logische operatoren</h3>

<p>
Condities kunnen gecombineerd worden via logische operatoren. 
<br/>
<br/>
Er zijn er drie:
</p>

<ul>
	<li>AND : <strong>&&</strong> of <strong>and</strong> </li>
	<li>OR: <strong>||</strong> of <strong>or</strong></li>
	<li>NOT: <strong>!</strong> </li>
	<li>XOR: <strong>xor</strong> </li>
</ul>

AND: waarbij conditie1 en conditie2 beide true zijn.
<blockquote>
	conditie1 && conditie2
</blockquote>
<br/>
OR: waarbij of conditie1 of conditie2 true is.
<blockquote>
	conditie1 || conditie2
</blockquote>
<br/>
NOT: waarbij true of false wordt omgekeerd.
<blockquote>
	!conditie1
</blockquote>
<br/>
XOR: waarbij conditie1 of conditie2 true is maar niet allebei.
<blockquote>
	conditie1 xor conditie2
</blockquote>

<h3>String operatoren</h3>
<p>
Strings aan elkaar plakken door middel van de punt operator: .
<blockquote>
	<br/>
	$hello_world = $hello . ' ' . $world;
	<br/>
	of 
	<br/>
	$hello_world .= $hello;
	<br/>
	$hello_world .= ' ';
	<br/>
	$hello_world .= $world;
	<br/>
</blockquote>
</p>

<h3>Voorbeeld: (zie code)</h3>
<?php 
$temperatuur = rand(-10, 40);

echo 'De temperatuur is: ' . $temperatuur . ' graden Celcius.';

echo '<br/>';

if ($temperatuur >= 10 && $temperatuur <= 20) {
    echo 'Milde temperatuur';
} elseif ($temperatuur < 10 ) {
    echo 'Koude temperatuur';
} else {
	echo 'Warme temperatuur';
}

echo '<br/>';

if (!$temperatuur) {
	echo 'Het is precies 0 graden';
} elseif($temperatuur < 0) {
	echo 'Het is kouder dan 0 graden';
} elseif($temperatuur > 0) {
	echo 'Het is warmer dan 0 graden';
}

echo '<br/>';

if($temperatuur < 0 || $temperatuur > 30) {
	echo 'De temperatuur is extreem';
}
?>
</blockquote>
</body>
</html> 