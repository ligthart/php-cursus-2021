<!DOCTYPE html>
<html>
<body>

<h1>Les 1.3 - Oefeningen</h1>

<p><em>* Je kunt de uitwerkingen plaatsen in een index.php bestand in de map "uitwerkingen".</em></p>

<h2>Oefening 1</h2>

<p>
Maak een programma waarmee je het BMI kan berekenen en tonen.
<br/>
De formule voor het berekenen van je BMI is: gewicht (kg) / (lengte (m) x lengte (m)).
<br/>
Regels:
</p>

<ul>
	<li>Lager dan 18,5: Ondergewicht</li>
	<li>Tussen 18,5 en 25: Gezond gewicht</li>
	<li>Tussen 25 en 30: Overgewicht</li>
	<li>Hoger dan 30: Obesitas</li>
</ul>

<h2>Oefening 2</h2>

<p>* Dit is optionele oefening; wanneer je tijd over hebt kun je hier aan beginnen.</p>

<p>

Maak een programma waarbij je controleert of de persoon kan worden toegelaten tot een feest:

Met de volgende regels:

</p>

<ul>
	<li>De naam mag niet "Pietje" zijn. Pietje is vervelend.</li>
	<li>Enkel volwassenen van boven de 18 jaar oud. </li>
	<li>Mogen geen relatie zijn.</li>
    <li>Moeten een t-shirt dragen met oranje of rood of wit of blauw als kleur want het thema is Holland.</li>
</ul>

</body>
</html> 