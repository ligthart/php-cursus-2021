<!DOCTYPE html>
<html>
<body>

<h1>Les 1.2 - Variabelen</h1>

<blockquote>
	&lt;php <br/>
	$title = "Hallo Wereld"; <br/>
	echo $title; <br/>
	?&gt;
</blockquote>

Regels:
<ul>
	<li>Een variable declareer je met een '$' teken</li>
	<li>Mag beginnen met een _, hoofd- of kleine letter</li>
	<li>Alleen de a t/m z, 0 t/m 9 en _ karakters zijn toegestaan</li>
</ul>

</body>
</html> 