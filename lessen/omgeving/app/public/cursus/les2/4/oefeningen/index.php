<!DOCTYPE html>
<html>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>

<h1>Les 2.4 - Oefeningen</h1>

<h2>Oefening 1</h2>

<p>
Maak een nieuwe tabel aan (via phpmyadmin) genaamd "posts" met de volgende structuur:
</p>

<ul>
	<li>id, Primary Key, BIGINT, AUTO_INCREMENT</li>
	<li>title, TEXT</li>
	<li>content, LONGTEXT</li>
	<li>status, VARCHAR(20)</li>
	<li>modified_date, DATETIME</li>
	<li>created_date, DATETIME</li>
</ul>

<h2>Oefening 2</h2>

<p>
Wijzig de comments tabel en voeg daar een kolom aan toe genaamd "post_id". 
Maak van deze kolom een "foreign key" die verwijst naar de kolom "id" in de tabel blog.posts. 
</p>

<p>
Hint: (Dit kan ook via phpMyAdmin).<br/>
Hint: Voor het toevoegen van een foreign key constraint: <br/>Zorg er voor dat je na het toevoegen van de kolom post_id er ook een record bestaat in de "posts" tabel. <br/>En zorg er voor dat alle comments de juiste post_id hebben.
</p>

<h2>Oefening 3</h2>
<p>
	Maak een formulier waarmee je post data kan toevoegen aan de database.
</p>	

<h2>Oefening 4</h2>
<p>
	Toon alle post data uit de database in een tabel.
</p>

<h2>Oefening 5</h2>
<p> 
	Maak het mogelijk om comments toe te voegen aan een post.
</p>

</body>
</html> 