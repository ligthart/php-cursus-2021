<!DOCTYPE html>
<html>
<head>
	<style>
	pre, code {
	  font-family: monospace, monospace;
	}
	pre {
	  overflow: auto;
	}
	pre > code {
	  display: block;
	  padding: 0rem;
	  word-wrap: normal;
	}
	</style>
</head>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>

<h1>Les 2.3</h1>

<h2>Constanten definieren</h2>

<p>
	Een constante kan niet veranderen tijdens de executie van een script en kunnen overal worden aangeroepen (global). Ze worden bij voorkeur met hoofdletters geschreven.
</p>

<p> Constanten zijn metname geschikt voor het opslaan van configuratie instellingen.
</p>
<pre>
    <code contenteditable spellcheck="false">

	define('DB_HOST',"localhost");
	
	</code>
</pre>


<h2>Connectie maken met de database</h2>

<p>
Hieronder voorbeeld code hoe je een connectie kan maken met de database.
</p>

<figure>
  <figcaption>Connectie maken met de database</figcaption>
  <pre>
    <code contenteditable spellcheck="false">
define('DB_HOST',"localhost");
define('DB_PORT', "8889"); // 8889 voor mamp mysql, 3306 voor docker mysql
define('DB_USERNAME',"root"); 
define('DB_PASSWORD',"root");
define('DB_DATABASE',"blog"); 

$connection = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE, DB_PORT);

if (!$connection) {
	die('Verbinding met de database is mislukt.');
}
    </code>
  </pre>
</figure>

<h2>Een query uitvoeren</h2>

<figure>
  <figcaption>Query uitvoeren en data ophalen</figcaption>
  <pre>
    <code contenteditable spellcheck="false">

$query = "SELECT * FROM comments ORDER BY created_date DESC";

$result = mysqli_query($connection, $query);

echo 'Number of comments:' . mysqli_num_rows($result);

$rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

foreach($rows as $row) {
	echo $row['id'];
	echo $row['name'];
	echo $row['email'];
	echo $row['website'];
	echo $row['created_date'];
	echo $row['message'];
}
    </code>
  </pre>
</figure>

<h2>Data toevoegen aan de database</h2>

Hieronder voorbeeld code hoe je een nieuw record kunt toevoegen een de comments tabel.

<figure>
  <figcaption>Data toevoegen aan de database</figcaption>
  <pre>
    <code contenteditable spellcheck="false">

$sql = "INSERT INTO comments (name, email, website, created_date, message) VALUES ('John Doe', 'john.doe@google.com', 'google.com', Now(), 'Hello World by John.')";

if (mysqli_query($connection, $sql) === TRUE) {
  echo "New record created successfully";
} else {
  echo "Error: " . $sql . "<br/>" . $connection->error;
}

    </code>
  </pre>
</figure>

</body>
</html> 