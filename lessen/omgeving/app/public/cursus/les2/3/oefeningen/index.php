<!DOCTYPE html>
<html>
<head>
	<style>
	pre, code {
	  font-family: monospace, monospace;
	}
	pre {
	  overflow: auto;
	}
	pre > code {
	  display: block;
	  padding: 0rem;
	  word-wrap: normal;
	}
	</style>
</head>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>	

<h1>Les 2.3 - Oefeningen</h1>

<h2>Oefening 1</h2>

<p>
Schrijf een functie waarmee de rijen en kolommen uit de comments tabel van de database in een html tabel worden getoond.
</p>

<h2>Oefening 2</h2>

<p>
Schrijf een functie waarmee je nieuwe records kan toevoegen aan de comments tabel.
</p>

<h2>Oefening 3</h2>

<p>
Neem het formulier uit 2.1 en maak het zo dat er bij iedere form submit een nieuw record wordt toegevoegd met de data die is ingevoerd in de velden.
</p>

</body>
</html> 