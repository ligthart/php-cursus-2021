<!DOCTYPE html>
<html>
<body>

<h1>Les 2 - 02 April 2021</h1>

<h2>Theorie</h2>
<ul>
	<li>
		<a href="/cursus/les2/1" title="Theorie 1">Theorie 1</a>
	</li>
	<li>
		<a href="/cursus/les2/2" title="Theorie 2">Theorie 2</a>
	</li>
	<li>
		<a href="/cursus/les2/3" title="Theorie 3">Theorie 3</a>
	</li>
	<li>
		<a href="/cursus/les2/4" title="Theorie 4">Theorie 4</a>
	</li>
	<li>
		<a href="/cursus/les2/5" title="Theorie 5">Theorie 5</a>
	</li>	
</ul>

<h2>Oefeningen</h2>
<ul>
	<li>
		<a href="/cursus/les2/1/oefeningen" title="Oefening 1">Oefening 1</a>
	</li>
	<li>
		<a href="/cursus/les2/2/oefeningen" title="Oefening 2">Oefening 2</a>
	</li>
	<li>
		<a href="/cursus/les2/3/oefeningen" title="Oefening 3">Oefening 3</a>
	</li>
	<li>
		<a href="/cursus/les2/4/oefeningen" title="Oefening 4">Oefening 4</a>
	</li>
	<li>
		<a href="/cursus/les2/5/oefeningen" title="Oefening 5">Oefening 5</a>
	</li>	
</ul>

<h2>Opdracht</h2>

<a href="/cursus/les2/opdracht">Opdracht</a>

</body>
</html>