<!DOCTYPE html>
<html>
<head>
	<style>
	pre, code {
	  font-family: monospace, monospace;
	}
	pre {
	  overflow: auto;
	}
	pre > code {
	  display: block;
	  padding: 0rem;
	  word-wrap: normal;
	}
	</style>
</head>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>

<h1>Les 2.5 - Sessions</h1>

<h2>Sessie starten</h2>

Wanneer je een sessie start kun je de waarden uit de globale variabele $_SESSION op iedere pagina raadplegen en zo kun je checken of een gebruiker bijvoorbeeld is ingelogd. En je kan evt nog andere data meegeven.

<figure>
  <figcaption>Voorbeeld code</figcaption>
  <pre>
    <code contenteditable spellcheck="false">

session_start();

$_SESSION['username'] = 'Piet';
$_SESSION['loggedin'] = true;
$_SESSION['time']     = time();

   </code>
  </pre>
</figure>

<h2>Sessie stoppen</h2>

Wanneer je een sessie wilt stoppen dan kun je onderstaande code aanroepen om de data uit de sessie te verwijderen. Dit doe je bijvoorbeeld wanneer de gebruiker uitlogd.

<figure>
  <figcaption>Voorbeeld code</figcaption>
  <pre>
    <code contenteditable spellcheck="false">

session_start();

$_SESSION = array();

session_destroy();

   </code>
  </pre>
</figure>

</body>
</html> 