<!DOCTYPE html>
<html>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>

<h1>Les 2.2 - Oefeningen</h1>

<h2>Oefening 1 - Maak database aan en tabel aan</h2>

<h3>DOCKER:</h3>
<p>
Open PHPMYADMIN, dat kan via je docker installatie (check dan wel even de nieuw code uit git via de github desktop client):
<br/><br/>
open: <strong>http://localhost:8081/index.php</strong>
<br/>

<h3>MAMP:</h3>

<strong>http://localhost:8888/MAMP/?language=English</strong>
<br/>
<br/>
Kies uit het "tools" menu phpMyAdmin.
</p>

<ol>
	<li>Maak een database aan genaamd "blog"</li>
	<li>Maak een tabel aan genaamd "comments"</li>
</ol>

<p>
De comments tabel bevat de volgende kolommen:
</p>

<ul>
	<li>id (INT), Index: Primary, A_I (autoincrement) aangevinkt</li>
	<li>name (VARCHAR), Length: 50</li>
	<li>email (VARCHAR), Length: 50 </li>
	<li>website (VARCHAR), Length: 255 </li>
	<li>created_date (DATETIME)</li>
	<li>message (TEXT)</li>
</ul>

<h2>Oefening 2 - Voeg een record toe</h2>
<p>
Met behulp van de knop “insert” is het mogelijk om data in de tabel op te slaan.
</p>
<p>
In de kolom “value” kun je voor elke tabelkolom een betreffende waarde invullen.
</p>

<h2>Oefening 3 - De waarde van de tabel bekijken</h2>
<p>
Aangezien je nu een of meer berichten hebt toegevoegd met de hiervoor beschreven methode, kun je nu op de knop “browse” achter je gastenboek klikken om de inhoud van de tabel te bekijken.
</p>

<h2>Oefening 4 - Voer een select query uit</h2>
<p>
Ga naar de SQL knop en voer een select query uit:

Schrijf een select query waarbij je zoekt op een specifieke "name" (in de WHERE clausule).
</p>

<h2>Oefening 5 - Schrijf update statement</h2>
<p>
Ga naar de SQL knop en voer een update statement uit.

Schrijf een update statement om de "message" te vervangen voor iets anders voor een specifieke gebruiker.
</p>

</body>
</html> 