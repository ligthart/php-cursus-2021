<!DOCTYPE html>
<html>
<head>
	<style>

	body {font-family: Arial, Helvetica, sans-serif;}
	* {box-sizing: border-box;}

	input[type=text], select, textarea {
	  width: 100%;
	  padding: 12px;
	  border: 1px solid #ccc;
	  border-radius: 4px;
	  box-sizing: border-box;
	  margin-top: 6px;
	  margin-bottom: 16px;
	  resize: vertical;
	}

	input[type=submit] {
	  background-color: #4CAF50;
	  color: white;
	  padding: 12px 20px;
	  border: none;
	  border-radius: 4px;
	  cursor: pointer;
	}

	input[type=submit]:hover {
	  background-color: #45a049;
	}

	pre, code {
	  font-family: monospace, monospace;
	}
	pre {
	  overflow: auto;
	}
	pre > code {
	  display: block;
	  padding: 0rem;
	  word-wrap: normal;
	}

	</style>
</head>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>

<h1>Les 2.1</h1>

<h2>Include statement</h2>

<p>
	Door gebruik te maken van het include statement kun je stukken code hergebruiken. Je kan bijvoorbeeld een standaard header, menu, en footer component aanmaken voor al je webpagina's.
</p>

Het statement werkt als volgt:

<pre>
	<code>
		include 'bestand.php';
	</code>
</pre>

<p>

Zie de code in voorbeeld.php. De herbruikbare componenten staan in de map "templates".
</p>
<p>
	<a href="voorbeeld.php">Resultaat voorbeeld gebruik include statement</a>.
</p>

<h2>Forms</h2>

<p>
	HTML formulieren zijn een belangrijke schakel tussen een gebruiker en een website. Hiermee kun je gegevens naar websites te sturen.
</p>

<p>
	Een HTML formulier bestaat uit een aantal elementen. Bijvoorbeeld tekstvelden (bestaande uit één of meerdere regels), keuzelijsten, knoppen of radioknoppen.
</p>

<p>
Voorbeeld HTML Contact Formulier:
</p>
<form action="index.php" method="post">
    <label for="name">Name</label>
    <input type="text" id="name" name="name" placeholder="Your name..">

    <label for="email">Email</label>
    <input type="text" id="email" name="email" placeholder="Your email">

  	 <label for="website">Website</label>
    <input type="text" id="website" name="website" placeholder="Your website url">

    <label for="message">Message</label>

    <textarea id="message" name="message" placeholder="Write something.." style="height:200px"></textarea>

    <input type="submit" value="Submit">
 </form>

<h2>PHP $_POST variable</h2>

<p>
$_POST is een PHP super global variabele, hierin worden de waardes gestopt van het verzonden html formulier.
</p>

Zie voorbeeld in code:

<p>
Naam: <?php echo $_POST['name']; ?> <br/>
E-mail: <?php echo $_POST['email']; ?> <br/>
Website: <?php echo $_POST['website']; ?> <br/>
Message: <?php echo $_POST['message']; ?> <br/>
</p>

<h2>GET Request</h2>
<p>
In de super global variabele $_GET worden alle waardes gestopt die middels
url parameters aan de pagina worden verzonden.
</p>
<p>
<a href="voorbeeld_get.php?variabele1=hallo&variabele2=wereld">Klik hier voor voorbeeld doorgeven url parameters</a>
</p>

</body>
</html> 