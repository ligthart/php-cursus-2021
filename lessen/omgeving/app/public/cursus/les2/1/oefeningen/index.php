<!DOCTYPE html>
<html>
<head>
	<style>
	pre, code {
	  font-family: monospace, monospace;
	}
	pre {
	  overflow: auto;
	}
	pre > code {
	  display: block;
	  padding: 0rem;
	  word-wrap: normal;
	}
	</style>
</head>
<body>

<a href="/cursus/les2">Terug naar overzicht</a>

<h1>Les 2.1 - Oefeningen</h1>

<h2>Oefening 1 - Include statement</h2>
<p>
Maak verschillende bestanden aan voor met daarin een header, menu en footer en include deze in een bestand genaamd index.php.
<br/>
Zie <a href="/cursus/les2/1/">theorie voor een voorbeeld</a>.
</p>

<h2>Oefening 2 - Form post</h2>

<ul>
	<li>name</li>
	<li>email</li>
	<li>website</li>
	<li>message</li>
</ul>

<ol>
	<li>Maak een html bestand aan genaamd contact.php met daarin een formulier met de bovenstaande velden.</li>
	<li>Zet de header, menu en footer om het formulier heen met het include statement.</li>
	<li>Zorg ervoor dat er een link in het menu staat naar contact.php</li>
	<li>Maak een bestand aan genaamd comment.php en zet die in de form action.</li>
	<li>Toon de waarden van de velden op het scherm na een form submit middels een foreach loop op de $_POST variabele in het comment.php bestand.</li>
</ol>

</body>
</html> 