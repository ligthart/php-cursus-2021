<?php

// Voorbeeld gebruik $_GET variabele.

include 'templates/header.php'; 

include 'templates/menu.php';

echo '<p>Zie de url parameters die zijn toevoegd aan de url hierboven in de locatiebalk. <br/>Probeer de waarden van de variabelen te veranderen in de URL en zie wat er gebeurd.</p>';

foreach($_GET as $key=>$value) {
	echo $key . '=' . $value;
	echo '<br/>';
}

include 'templates/footer.php';
?>